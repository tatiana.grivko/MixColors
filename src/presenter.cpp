#include "presenter.h"
#include "rayssum.h"

Presenter::Presenter(QObject *parent)
    : QObject(parent)
    , m_raysModel(new RaysModel(this))
{

}

void Presenter::calculateSum()
{
    setSumColor(RaysSum::calculateSum(m_raysModel->rays()));
}

QColor Presenter::sumColor() const
{
    return m_sumColor;
}

void Presenter::setSumColor(const QColor &sumColor)
{
    if (m_sumColor == sumColor)
        return;

    m_sumColor = sumColor;
    emit sumColorChanged();
}

RaysModel *Presenter::raysModel() const
{
    return m_raysModel;
}

int RaysModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_rays.count();
}

QVariant RaysModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());
    const int row = index.row();
    Q_ASSERT(row >= 0 && row < rowCount());
    const auto ray = m_rays.at(row);

    switch (role) {
    case Frequency:
        return ray.frequency();
    case Intensity:
        return ray.intensity();
    case Color: {
        return QColor::fromHsvF(RaysSum::frequencyToHue(ray.frequency()), 1, ray.intensity());
    }
    default:
        return {};
    }
}

QHash<int, QByteArray> RaysModel::roleNames() const
{
    return { { Frequency, "frequency" },
        { Intensity, "intensity" },
        { Color, "color" } };
}

QList<Ray> RaysModel::rays()
{
    return m_rays;
}

void RaysModel::addRay(double frequency, double intensity)
{
    const int afterLast = m_rays.count();
    beginInsertRows(QModelIndex(), afterLast, afterLast);
    m_rays.append({frequency, intensity});
    endInsertRows();
}

void RaysModel::removeRay(int index)
{
    Q_ASSERT(index >= 0 && index < rowCount());
    beginRemoveRows(QModelIndex(), index, index);
    m_rays.removeAt(index);
    endRemoveRows();
}

void RaysModel::setFrequency(int index, double frequency)
{
    Q_ASSERT(index >= 0 && index < rowCount());
    m_rays[index].setFrequency(frequency);
    QModelIndex modelIndex = this->index(index, 0);
    dataChanged(modelIndex, modelIndex, {Frequency, Color});
}

void RaysModel::setIntensity(int index, double intensity)
{
    m_rays[index].setIntensity(intensity);
    QModelIndex modelIndex = this->index(index, 0);
    dataChanged(modelIndex, modelIndex, {Intensity, Color});
}
