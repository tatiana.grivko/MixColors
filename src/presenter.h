#pragma once

#include <lib/rayssum.h>

#include <QAbstractListModel>
#include <QObject>
#include <QColor>

class RaysModel : public QAbstractListModel
{
    Q_OBJECT
public:
    using QAbstractListModel::QAbstractListModel;

    enum Roles {
        Frequency = Qt::DisplayRole + 1,
        Intensity,
        Color
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    QList<Ray> rays();

    Q_INVOKABLE void addRay(double frequency, double intensity);
    Q_INVOKABLE void removeRay(int index);
    Q_INVOKABLE void setFrequency(int index, double frequency);
    Q_INVOKABLE void setIntensity(int index, double intensity);

private:
    QList<Ray> m_rays;
};

class Presenter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor sumColor READ sumColor WRITE setSumColor NOTIFY sumColorChanged)
    Q_PROPERTY(RaysModel *raysModel READ raysModel CONSTANT)
public:
    explicit Presenter(QObject *parent = nullptr);

    Q_INVOKABLE void calculateSum();

    QColor sumColor() const;
    void setSumColor(const QColor &sumColor);

    RaysModel *raysModel() const;

signals:
    void sumColorChanged();

private:
    QColor m_sumColor;
    RaysModel *m_raysModel = nullptr;
};
