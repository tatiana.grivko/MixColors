#include "rayssum.h"

#include "mathhelper.h"

namespace  {
const double minHue = 0.0; // red
const double maxHue = 0.8; // violet
}

Ray::Ray(double frequency, double intensity)
{
    setFrequency(frequency);
    setIntensity(intensity);
}

double Ray::frequency() const
{
    return m_frequency;
}

void Ray::setFrequency(double value)
{
    m_frequency = MathHelper::crop(value, minFrequency, maxFrequency);
}

double Ray::intensity() const
{
    return m_intensity;
}

void Ray::setIntensity(double value)
{
    m_intensity = MathHelper::crop(value, minIntensity, maxIntensity);
}

RaysSum::RaysSum(QObject *parent) : QObject(parent)
{

}

QColor RaysSum::calculateSum(const QList<Ray> &rays)
{
    const auto n = rays.count();
    double sumR = 0, sumG = 0, sumB = 0;
    double sumIntensity = 0;
    for (int i = 0; i < n; i++) {
        auto _item = rays.at(i);
        auto _hue = frequencyToHue(_item.frequency());
        auto _intensity = _item.intensity();
        QColor _color = QColor::fromHsvF(_hue, 1, _intensity);
        sumR += _color.redF();
        sumG += _color.greenF();
        sumB += _color.blueF();
        sumIntensity += _intensity;
    }
    QColor color = QColor::fromRgbF(sumR / n, sumG / n, sumB / n);
    return QColor::fromHsvF(color.hueF(), color.saturationF(), sumIntensity / n);
}

double RaysSum::hueToFrequency(double hue)
{
    return MathHelper::convertRanges(hue, minHue, maxHue, Ray::minFrequency, Ray::maxFrequency);
}

double RaysSum::frequencyToHue(double frequency)
{
    return MathHelper::convertRanges(frequency, Ray::minFrequency, Ray::maxFrequency, minHue, maxHue);
}
