#include "mathhelper.h"

#include <QtMath>

MathHelper::MathHelper()
{

}

double MathHelper::crop(double value, double min, double max)
{
    return qMin(max, qMax(min, value));
}

double MathHelper::toRange(double value, double min, double max)
{
    return min + crop(value, 0., 1.) * (max - min);
}

double MathHelper::fromRange(double value, double min, double max)
{
    return (crop(value, min, max) - min) / (max - min);
}

double MathHelper::convertRanges(double value, double min, double max, double newMin, double newMax)
{
    return toRange(fromRange(value, min, max), newMin, newMax);
}
