#pragma once

#include <QObject>
#include <QColor>

class Ray {
public:
    Ray(double frequency, double intensity);

    double frequency() const;
    void setFrequency(double value);

    double intensity() const;
    void setIntensity(double value);

    constexpr static double minFrequency = 400; //THz
    constexpr static double maxFrequency = 700; //THz

    constexpr static double minIntensity = 0.;
    constexpr static double maxIntensity = 1.;

private:
    double m_frequency;
    double m_intensity;
};

class RaysSum : public QObject
{
public:
    explicit RaysSum(QObject *parent = nullptr);

    static QColor calculateSum(const QList<Ray> &rays);

    static double hueToFrequency(double hue);
    static double frequencyToHue(double frequency);
};
