#pragma once

class MathHelper
{
public:
    MathHelper();

    static double crop(double value, double min, double max);
    static double toRange(double value, double min, double max);
    static double fromRange(double value, double min, double max);
    static double convertRanges(double value, double min, double max,
                                      double newMin, double newMax);
};
