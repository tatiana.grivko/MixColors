#pragma once

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(GuiLibraryCategory)

class QQmlEngine;

class GuiLibrary {
public:
    static void initLibrary(QQmlEngine* engine);
};
