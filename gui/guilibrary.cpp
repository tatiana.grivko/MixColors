#include "guilibrary.h"

#include <QQmlEngine>

Q_LOGGING_CATEGORY(GuiLibraryLibraryCategory, "GuiLibraryLibraryCategory")

void GuiLibrary::initLibrary(QQmlEngine* engine)
{
    Q_INIT_RESOURCE(resources);
    engine->addImportPath(":/");

    qmlRegisterType(QUrl("qrc:/components/TextButton.qml"), "Gui", 1, 0, "TextButton");
    qmlRegisterType(QUrl("qrc:/components/TextInput_WithLabel.qml"), "Gui", 1, 0, "TextInput_WithLabel");
    qmlRegisterType(QUrl("qrc:/components/TransparentIconButton.qml"), "Gui", 1, 0, "TransparentIconButton");

    qCDebug(GuiLibraryLibraryCategory) << "Gui library init done";
}
