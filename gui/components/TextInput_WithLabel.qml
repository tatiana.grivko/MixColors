import QtQuick 2.6
import QtQuick.Layouts 1.3

Item {
    id: root

    property alias label: label.text

    property bool inputError: false
    property alias input_echoMode: input.echoMode
    property alias input_inputMask: input.inputMask
    property alias input_inputMethodHints: input.inputMethodHints
    property alias input_inputText: input.text
    property alias input_passwordCharacter: input.passwordCharacter

    signal input_editingFinished(string text) /*input.editingFinished*/
    signal input_accepted(string text) /*input.accepted*/
    signal input_textChanged(string text) /*input.textChanged*/

    implicitHeight: column.height
    implicitWidth: 150

    ColumnLayout {
        id: column
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
        spacing: 3

        Text {
            id: label
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: root.inputError ? "#FF0000" : "#1E1E1E"
            font.family: "Lato"
            font.pixelSize: 14
            text: "caption"
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        Rectangle {
            id: background
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            border.color: {
                if (root.inputError)
                    return "#FF0000"
                if (input.activeFocus)
                    return "#1E1E1E"
                return "#343434"
            }
            border.width: root.inputError || input.activeFocus ? 2 : 1
            color: "#E0E0E0"
            radius: 6



            TextInput {
                id: input
                anchors {
                    fill: parent
                    margins: 10
                }
                clip: true
                color: "black"
                font.family: "Lato"
                font.pixelSize: 20
                font.bold: input.activeFocus
                verticalAlignment: Text.AlignVCenter
                selectByMouse: true
                onAccepted: {
                    root.input_accepted(text)
                }
                onEditingFinished: {
                    root.input_editingFinished(text)
                }
                onTextChanged: {
                    root.input_textChanged(text)
                }
                onActiveFocusChanged:{
                    if (activeFocus)
                        selectAll()
               }
            }
        }
    }
}
