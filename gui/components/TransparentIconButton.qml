import QtQuick 2.6

Item {
    id: root

    property alias icon_source: icon.source

    signal clicked() /*__mouseArea.clicked*/
    signal doubleClicked() /*__mouseArea.doubleClicked*/
    signal pressed() /*__mouseArea.pressed*/
    signal released() /*__mouseArea.released*/

    implicitWidth: 50
    implicitHeight: 50

    Image {
        id: icon
        anchors.centerIn: parent
        clip: true
        source: "qrc:/icons/cross.svg"
        width: 30
        height: 30
        sourceSize.width: width
        sourceSize.height: height
    }

    Rectangle {
        id: tint
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.3)
        visible: __mouseArea.pressed
        radius: 6
    }

    MouseArea {
        id: __mouseArea
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        z: -1

        onClicked: {
            root.clicked();
        }
        onDoubleClicked: {
            root.doubleClicked();
        }
        onPressed: {
            root.pressed();
        }
        onReleased: {
            root.released();
        }
    }

}
