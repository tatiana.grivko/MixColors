import QtQuick 2.6

Item {
    id: root

    property alias text: text.text

    property bool isPrimary: true

    signal clicked() /*__mouseArea.clicked*/
    signal doubleClicked() /*__mouseArea.doubleClicked*/
    signal pressed() /*__mouseArea.pressed*/
    signal released() /*__mouseArea.released*/

    height: 50
    width: 200

    MouseArea {
        id: __mouseArea
        anchors.fill: parent
        z: -1

        onClicked: {
            root.clicked()
        }
        onDoubleClicked: {
            root.doubleClicked()
        }
        onPressed: {
            forceActiveFocus()
            root.pressed()
        }
        onReleased: {
            root.released()
        }
    }
    BorderImage {
        id: background
        anchors.fill: parent
        border.bottom: 10
        border.left: 10
        border.right: 10
        border.top: 10
        source: root.isPrimary ? "qrc:/icons/primary_button_background.png"
                               : "qrc:/icons/secondary_button_background.png"
    }
    Text {
        id: text
        anchors.fill: parent
        anchors.margins: 10
        font.family: "Lato"
        font.pixelSize: 28
        font.styleName: "Button"
        horizontalAlignment: Text.AlignHCenter
        text: qsTr("Button")
        color: root.isPrimary ? "#FFFFFF" : "#3683CB"
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        z: 1
        elide: Text.ElideRight
    }

    Rectangle {
        id: tint
        anchors.fill: parent
        radius: 10
        color: "#000000"
        opacity: {
            if (!root.enabled )
                return 0.6
            if (__mouseArea.pressed)
                return 0.3
            return 0
        }
    }

}
