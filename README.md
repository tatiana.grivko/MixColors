# MixColors

Simple Qt application for calculating a sum of visible rays in the meaning of a human eye perception. 



## Input data

Each ray is represented by two parameters
- frequency (400 - 700 THz)
- intensity (0 - 1) 

There can be any number of rays.

## Output data

The result of calculation is the displayed color.

For example, for pure Red, Green and Blue input rays (400 THz, 525 THz and 650 THz, respectively) of the maximum intensity, the result color will be pure white.

## Method

HSV model is used for color calculations. 

![HSV_cylinder](illustrations/HSV_cylinder.png)

## Environment requirements

The project requires Qt 5.12 or greater, C++ compiler, and CMake. 

Should be cross-platform.


