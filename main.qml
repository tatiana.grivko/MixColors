import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import Gui 1.0

Window {
    visible: true
    minimumWidth: 400
    minimumHeight: 360
    width: 640
    height: 480
    title: qsTr("Mix Colors")


    ColumnLayout {
        anchors {
            fill: parent
            margins: 20
        }

        spacing: 20

        TextButton {
            Layout.fillWidth: true
            text: qsTr("Add color")
            onClicked: presenter.raysModel.addRay(400, 1)
        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: 5
            Layout.maximumHeight: contentHeight
            Layout.preferredHeight: contentHeight
            spacing: 25
            clip: true
            model: presenter.raysModel
            delegate: RowLayout {
                anchors {
                    left: parent ? parent.left : undefined
                    right: parent ? parent.right : undefined
                }
                spacing: 10
                TextInput_WithLabel {
                    Layout.fillWidth: true
                    label: qsTr("Frequency, THz")
                    input_inputText: frequency // modelBound
                    onInput_editingFinished: {
                        var value = parseFloat(input_inputText)
                        var error = isNaN(value)
                        inputError = error
                        presenter.raysModel.setFrequency(index, value)
                    }
                }
                TextInput_WithLabel {
                    Layout.fillWidth: true
                    label: qsTr("Intensity, fractions of a unit")
                    input_inputText: intensity // modelBound
                    onInput_editingFinished: {
                        var value = parseFloat(input_inputText)
                        var error = isNaN(value)
                        inputError = error
                        presenter.raysModel.setIntensity(index, value)
                    }
                }
                Rectangle {
                    Layout.alignment: Qt.AlignBottom
                    Layout.fillWidth: false
                    Layout.minimumWidth: 50
                    Layout.minimumHeight: 50
                    color: model["color"] // modelBound
                    border.color: "black"
                    radius: 6
                }

                TransparentIconButton {
                    Layout.alignment: Qt.AlignBottom
                    Layout.rightMargin: 5
                    onClicked: presenter.raysModel.removeRay(index)
                }
            }
            ScrollBar.vertical: ScrollBar {
                property bool fits: listView.contentHeight <= listView.height
                policy: fits ? ScrollBar.AsNeeded : ScrollBar.AlwaysOn
            }
        }

        TextButton {
            Layout.fillWidth: true
            text: qsTr("Calculate mix")
            onClicked: {
                presenter.calculateSum()
            }
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.minimumHeight: 50
            Layout.fillWidth: true
            border.color: "black"
            color: presenter.sumColor
        }
    }
}
